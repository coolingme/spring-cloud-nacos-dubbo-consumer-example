package com.coolingme.springcloud.consumer;

import com.coolingme.springcloud.provider.api.ISayHelloService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangyue
 * @date 2022/10/05 17:58
 */
@RestController
public class NacosController {

    /**
     * 启动项目时不检查这个服务
     */
    @Reference(check = false)
    ISayHelloService sayHelloService;

    /**
     * 调用提供者服务的接口
     */
    @GetMapping("/call")
    public String callProvider() {
        return this.sayHelloService.sayHello("Joey");
    }

}
