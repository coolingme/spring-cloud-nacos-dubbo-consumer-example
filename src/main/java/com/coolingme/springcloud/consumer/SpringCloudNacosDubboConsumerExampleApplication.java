package com.coolingme.springcloud.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCloudNacosDubboConsumerExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudNacosDubboConsumerExampleApplication.class, args);
    }

}
